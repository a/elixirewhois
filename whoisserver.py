#!/usr/bin/env python3

import time
import socket
import requests
from config import *

HOST = "0.0.0.0"
PORT = 43
domain_cache = []
whois_cache = []
last_cache = 0


def whois_style(data):
    if not data:
        return "\r\n%% No entries found."

    whois_text = ""
    for entry in data:
        value = data[entry]

        # Turn booleans into YES/NO
        if value is True:
            value = "YES"
        elif value is False:
            value = "NO"

        whois_text += f"\r\n{entry}: {value}"
    return whois_text


def get_domains():
    global domain_cache
    global last_cache
    domain_cache = requests.get(elixire_instance + "/api/admin/domains",
                                headers={"Authorization": api_key}).json()
    last_cache = time.time()


def get_domain_info(domain):
    # check if cache exists or not or if it can be pulled again
    if not domain_cache or time.time() - last_cache > cache_validity:
        get_domains()

    if domain in whois_cache:
        return whois_cache[domain]

    domain = domain.replace("*.", "")

    # Get domain info from domain name
    data = [domain_cache[did]['info'] for did in domain_cache if
            domain_cache[did]['info']['domain'].replace("*.", "") == domain]

    # Account for no data or admin only domains
    if not data or data[0]["admin_only"]:
        return whois_style(False)
    else:
        data = data[0]

    # Prepare a baseline for whois data
    whois_data = {"domain": domain,
                  "status": "ACTIVE"}

    username = data["owner"]["username"]

    # Check if we can show the username, if we can, show it
    # if not, anonymize it
    if (data["owner"]["paranoid"] and username not in user_whitelist)\
            or username in user_blacklist:
        whois_data["owner"] = "anonymous"
        whois_data["anonymous"] = True
    else:
        whois_data["owner"] = username
        whois_data["anonymous"] = False

    # add official field
    whois_data["official"] = data["official"]

    # Style the outcome
    whois_data = whois_style(whois_data)

    # Cache the output and return it
    whois_cache.append(whois_data)
    return whois_data


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    while True:
        conn, addr = s.accept()
        with conn:
            try:
                conn.sendall(whois_intro.encode())
                print('Connected by', addr)
                conn.settimeout(1)
                data = conn.recv(1024)
                domain = data.decode().split("\r\n")[0]

                if not domain:
                    conn.sendall(b"%% No input given")
                else:
                    print('domain', domain)
                    conn.sendall(get_domain_info(domain).encode())
                conn.close()
            except socket.timeout:
                conn.sendall(b"\r\n%% Timed out, goodbye!")
                conn.close()
            except:
                conn.sendall(b"\r\n%% Something went wrong. "
                             b"Please try again and open an issue on the "
                             b"git repo if the issue keeps happening!")
                conn.close()
    s.close()
    dbcur.close()
    dbconn.close()
